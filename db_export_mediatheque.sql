-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 22 Avril 2017 à 17:42
-- Version du serveur :  10.1.13-MariaDB
-- Version de PHP :  7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mediatheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `articleID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `disponible` int(11) NOT NULL DEFAULT '1',
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`articleID`, `title`, `disponible`, `type`) VALUES
(31, 'Harry Potter', 1, 'livre'),
(32, 'Triologie Harry Potter', 1, 'livreInfo'),
(37, 'Le monde de Théo', 1, 'livre'),
(38, 'Sobhj', 1, 'livre');

-- --------------------------------------------------------

--
-- Structure de la table `articleauthors`
--

CREATE TABLE `articleauthors` (
  `articleAuthorsID` int(11) NOT NULL,
  `articleID` int(11) NOT NULL,
  `personID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `articleauthors`
--

INSERT INTO `articleauthors` (`articleAuthorsID`, `articleID`, `personID`) VALUES
(44, 31, 43),
(45, 31, 44),
(46, 32, 43),
(47, 33, 45),
(48, 33, 46),
(49, 34, 45),
(50, 35, 47),
(51, 35, 48),
(52, 36, 47),
(53, 37, 49),
(54, 38, 50);

-- --------------------------------------------------------

--
-- Structure de la table `articlemeta`
--

CREATE TABLE `articlemeta` (
  `articleMetaID` bigint(20) NOT NULL,
  `articleID` bigint(20) UNSIGNED NOT NULL,
  `metaKey` varchar(255) DEFAULT NULL,
  `metaValue` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `articlemeta`
--

INSERT INTO `articlemeta` (`articleMetaID`, `articleID`, `metaKey`, `metaValue`) VALUES
(49, 31, 'editeur', 'Flamarion'),
(50, 31, 'max_author_nb', '3'),
(51, 32, 'editeur', 'Flamarion edition'),
(52, 32, 'max_author_nb', '2'),
(53, 33, 'editeur', 'Flamarion'),
(54, 33, 'max_author_nb', '3'),
(55, 34, 'editeur', 'Flamarion edition'),
(56, 34, 'max_author_nb', '2'),
(57, 35, 'editeur', 'Flamarion'),
(58, 35, 'max_author_nb', '3'),
(59, 36, 'editeur', 'Flamarion edition'),
(60, 36, 'max_author_nb', '2'),
(61, 37, 'editeur', 'Démaggo'),
(62, 37, 'max_author_nb', '3'),
(63, 38, 'editeur', 'efret'),
(64, 38, 'max_author_nb', '3');

-- --------------------------------------------------------

--
-- Structure de la table `person`
--

CREATE TABLE `person` (
  `personID` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `workmade` int(11) NOT NULL,
  `personType` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `person`
--

INSERT INTO `person` (`personID`, `firstname`, `lastname`, `workmade`, `personType`) VALUES
(43, 'JK.', 'Rowling', 2, 'author'),
(44, 'Jean', 'Rostand', 1, 'author'),
(45, 'JK.', 'Rowling', 2, 'author'),
(46, 'Jean', 'Rostand', 1, 'author'),
(47, 'JK.', 'Rowling', 2, 'author'),
(48, 'Jean', 'Rostand', 1, 'author'),
(49, 'JK', 'Rowling', 1, 'author'),
(50, 'JK', 'Rowling', 1, 'author');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(30) NOT NULL,
  `bookBorrowable` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`userID`, `city`, `address`, `name`, `type`, `bookBorrowable`) VALUES
(35, 'Corbeil-Essonnes', 'Route de Lisses', 'Florian TIAR', 'student', 5),
(36, 'Kremlin Bicêtre', 'avenue générale de gaule', 'Roget', 'teacher', 10),
(37, 'Corbeil-Essonnes', 'Route de Lisses', 'Florian TIAR', 'student', 5),
(38, 'Kremlin Bicêtre', 'avenue générale de gaule', 'Roget', 'teacher', 10),
(39, 'Corbeil-Essonnes', 'Route de Lisses', 'Florian TIAR', 'student', 5),
(40, 'Kremlin Bicêtre', 'avenue générale de gaule', 'Roget', 'teacher', 10);

-- --------------------------------------------------------

--
-- Structure de la table `userarticle`
--

CREATE TABLE `userarticle` (
  `userarticleID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `articleID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `userarticle`
--

INSERT INTO `userarticle` (`userarticleID`, `userID`, `articleID`) VALUES
(1, 37, 33),
(2, 39, 35);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`articleID`);

--
-- Index pour la table `articleauthors`
--
ALTER TABLE `articleauthors`
  ADD PRIMARY KEY (`articleAuthorsID`);

--
-- Index pour la table `articlemeta`
--
ALTER TABLE `articlemeta`
  ADD PRIMARY KEY (`articleMetaID`);

--
-- Index pour la table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`personID`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- Index pour la table `userarticle`
--
ALTER TABLE `userarticle`
  ADD PRIMARY KEY (`userarticleID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `articleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT pour la table `articleauthors`
--
ALTER TABLE `articleauthors`
  MODIFY `articleAuthorsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT pour la table `articlemeta`
--
ALTER TABLE `articlemeta`
  MODIFY `articleMetaID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT pour la table `person`
--
ALTER TABLE `person`
  MODIFY `personID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pour la table `userarticle`
--
ALTER TABLE `userarticle`
  MODIFY `userarticleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
