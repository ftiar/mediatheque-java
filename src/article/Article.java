package article;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import mediatheque.CustomException;
import mysql.Connect;
import person.Person;

public abstract class Article {
	
	private int articleID;
	private String titre;
	private String editeur;
	private String type;
	private ArrayList <Person> authors = new ArrayList<Person>();
	private boolean disponible;
	protected int nb_authors_max = 3; // Default value

	public Article(String titre, String editeur, Person person, String type ) {
		this.titre = titre;
		this.editeur = editeur;
		this.disponible = true;
		this.type = type;
		
		this.insertArticle(titre, type);
		this.insertArticleMeta("editeur", editeur);
		
		try {
			addAuthor(person);
		} catch (CustomException e) {
			e.printStackTrace();
		}
	}
	
	public Article(String titre, String editeur, String type ) {
		this.titre = titre;
		this.editeur = editeur;
		this.disponible = true;
		this.type = type;
		
		this.insertArticle(titre, type);
		this.insertArticleMeta("editeur", editeur);
	}
	
	public Article(String titre, String type ) {
		this.titre = titre;
		this.disponible = true;
		this.type = type;
		
		this.insertArticle(titre, type);
	}
	
	public int insertArticle( String titre, String type ) {
		PreparedStatement pst = null;
		ResultSet resultats = null;
		int idGenere = -1;
		
		try {
			Connection cnx = Connect.getConnexion();

	        String requeteSQL = "INSERT INTO article (title, disponible, type) values(?, ?, ?)";

	        System.out.println("before article creation");
	        pst = cnx.prepareStatement(requeteSQL, Statement.RETURN_GENERATED_KEYS);
	    
	        pst.setString(1, titre);
	        pst.setInt(2, 1);
	        pst.setString(3, type);

	        try {
	            pst.executeUpdate();
	            resultats = pst.getGeneratedKeys();
	            if (resultats.next()) {
	            	idGenere = resultats.getInt(1);
	            	this.articleID = idGenere;
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return idGenere;
	}
	
	public int insertArticleMeta( String metaKey, String metaValue ) {
		PreparedStatement pst = null;
		ResultSet resultats = null;
		int idGenere = -1;
		
		try {
			Connection cnx = Connect.getConnexion();
	        
	        String requeteSQL = "INSERT INTO articlemeta (articleID, metaKey, metaValue) values(?, ?, ?)";

	        System.out.println("before article creation");
	        pst = cnx.prepareStatement(requeteSQL, Statement.RETURN_GENERATED_KEYS);

	        pst.setInt(1, this.articleID);
	        pst.setString(2, metaKey);
	        pst.setString(3, metaValue);

	        try {
	            pst.executeUpdate();
	            resultats = pst.getGeneratedKeys();
	            if (resultats.next()) {
	            	idGenere = resultats.getInt(1);
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return idGenere;
	}
	
	public int insertArticleMeta( String metaKey, int metaValue ) {
		PreparedStatement pst = null;
		ResultSet resultats = null;
		int idGenere = -1;
		
		try {
			Connection cnx = Connect.getConnexion();
	        
	        String requeteSQL = "INSERT INTO articlemeta (articleID, metaKey, metaValue) values(?, ?, ?)";

	        System.out.println("before article creation");
	        pst = cnx.prepareStatement(requeteSQL, Statement.RETURN_GENERATED_KEYS);

	        pst.setInt(1, this.articleID);
	        pst.setString(2, metaKey);
	        pst.setInt(3, metaValue);

	        try {
	            pst.executeUpdate();
	            resultats = pst.getGeneratedKeys();
	            if (resultats.next()) {
	            	idGenere = resultats.getInt(1);
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return idGenere;
	}
	
    public static int deleteArticle(int  id) throws SQLException, ClassNotFoundException {
		Connection cnx = Connect.getConnexion();
        PreparedStatement pst = null;
        int nblignes = 0;

        String requeteSQL = "DELETE from article where articleID ="+id;

        System.out.println("before delete article");
        pst = cnx.prepareStatement(requeteSQL);   
            
        try {
            nblignes = pst.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        pst.close();
        cnx.close();
        return nblignes;
    }
    
	 public static ResultSet GetArticleList() throws ClassNotFoundException, SQLException
     {
         String req = "select * from article";
 		 Connection cnx = Connect.getConnexion();
       
         PreparedStatement pst = cnx.prepareStatement(req);
         ResultSet jeu = pst.executeQuery();
         
         return jeu;
     }
	 
	public int getArticleID() {
		return this.articleID;
	}
	
	public ArrayList <Person> getAuthors() {
		return this.authors;
	}
	
	public void addAuthor( Person person ) throws CustomException {
		if ( this.authors.size() == this.nb_authors_max ) {
			throw new CustomException( "Error : Maximum Person number for this article" );
		}
		this.authors.add(person);
		person.increaseWorkNumber();

		// Add to DB
		Connection cnx;
		try {
			cnx = Connect.getConnexion();
	        PreparedStatement pst = null;
	        int idGenere = -1;
			ResultSet resultats = null;
	        
	        String requeteSQL = "INSERT INTO articleauthors (articleID, personID) values(?, ?)";

	        System.out.println("before author creation");
	        pst = cnx.prepareStatement(requeteSQL, Statement.RETURN_GENERATED_KEYS);

	        pst.setInt(1, this.articleID);
	        pst.setInt(2, person.getPersonID());

	        try {
	            pst.executeUpdate();
	            resultats = pst.getGeneratedKeys();
	            if (resultats.next()) {
	            	idGenere = resultats.getInt(1);
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void removeAuthor( Person person ) throws CustomException {
		if ( this.authors.size() == 1 ) {
			throw new CustomException( "Error : This book needs one Person at least" );
		}
		this.authors.remove(person);
		person.decreaseWorkNumber();
		
		// Remove from DB
		Connection cnx;
		try {
			cnx = Connect.getConnexion();

	        PreparedStatement pst = null;

	        String requeteSQL = "DELETE from articleauthors where articleID ="+this.articleID + " AND personID = " + person.getPersonID();

	        System.out.println("before delete author from article");
	        pst = cnx.prepareStatement(requeteSQL);   
	            
	        try {
	            pst.executeUpdate();
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getEditeur() {
		return editeur;
	}

	public void setEditeur(String editeur) {
		this.editeur = editeur;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponibilite(boolean disponible) {
		this.disponible = disponible;
		PreparedStatement pst = null;
		try {
			Connection cnx = Connect.getConnexion();
			
	        String requeteSQL = "UPDATE article SET disponible=" + this.disponible + " WHERE articleID = " + this.articleID;

	        System.out.println("before set disponible boolean");
	        pst = cnx.prepareStatement(requeteSQL);

	        try {
	            pst.executeUpdate();
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public void setMaxAuthorNb( int maxAuthor ) {
		insertArticleMeta( "max_author_nb", maxAuthor );
		this.nb_authors_max = maxAuthor;
	}

	@Override
	public String toString() {
		return "Article [id=" + this.articleID + ", titre=" + titre + ", editeur=" + editeur + ", authors=" + authors + "]";
	}
	
	
}
