package article;

import mediatheque.CustomException;
import person.Person;

public class DVD extends Article {

	private String realisateur;

	public DVD( String titre, String real, Person acteur1, Person acteur2) {
		super(titre, "dvd");

		this.setMaxAuthorNb(3);
		this.realisateur = real;
		this.insertArticleMeta("realisateur", real);
		try {
			this.addAuthor(acteur1);
			this.addAuthor(acteur2);
		} catch (CustomException e) {
			e.printStackTrace();
		}

	}
	
	public DVD( String titre, String real, Person acteur1, Person acteur2, Person acteur3) {
		super(titre, "dvd");

		this.setMaxAuthorNb(3);
		this.realisateur = real;
		this.insertArticleMeta("realisateur", real);
		try {
			this.addAuthor(acteur1);
			this.addAuthor(acteur2);
			this.addAuthor(acteur3);
		} catch (CustomException e) {
			e.printStackTrace();
		}

	}
	
}
