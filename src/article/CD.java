package article;


import mediatheque.CustomException;
import person.Person;

public class CD extends Article {

	private String producteur;
	
	public CD( String titre, String prod, Person chanteur1, Person chanteur2) {
		super(titre, "cd");

		this.producteur = prod;
		this.insertArticleMeta("producteur", prod);
		try {
			this.addAuthor(chanteur1);
			this.addAuthor(chanteur2);
		} catch (CustomException e) {
			e.printStackTrace();
		}
	}
	
	public CD( String titre, String prod, Person chanteur1, Person chanteur2, Person chanteur3) {
		super(titre, "cd");

		this.producteur = prod;
		this.insertArticleMeta("producteur", prod);
		try {
			this.addAuthor(chanteur1);
			this.addAuthor(chanteur2);
			this.addAuthor(chanteur3);
		} catch (CustomException e) {
			e.printStackTrace();
		}
	}

}
