package article;

import mediatheque.CustomException;
import person.Person;

public class Livre extends Article {
	
	public Livre(String titre, String editeur, Person person) {
		super(titre, editeur, person, "livre");
		this.setMaxAuthorNb(3);
	}

	public Livre(String titre, String editeur, Person person, Person author2 ) {
		super(titre, editeur, person, "livre");
		this.setMaxAuthorNb(3);
		try {
			addAuthor(author2);
		} catch (CustomException e) {
			e.printStackTrace();
		}
	}

	public Livre(String titre, String editeur, Person person, Person author2, Person author3 ) {
		super(titre, editeur, person, "livre");
		this.setMaxAuthorNb(3);
		try {
			addAuthor(author2);
			addAuthor(author3);
		} catch (CustomException e) {
			e.printStackTrace();
		}
	}
	
}
