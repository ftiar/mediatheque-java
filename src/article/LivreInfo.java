package article;

import mediatheque.CustomException;
import person.Person;

public class LivreInfo extends Article {
	
	private int nbCD;

	public LivreInfo(String titre, String editeur, int nombreCD, Person person) throws CustomException {
		super(titre, editeur, person, "livreInfo");
		
		if ( nombreCD < 1 ) {
			throw new CustomException( "ebooks needs 1 CD at least" );
		}
		this.nbCD = nombreCD;
		this.setMaxAuthorNb(2); // 2 auteurs max par Livre d'informatique, algorithme g�r� dans la classe Article
	}
	
	public LivreInfo(String titre, String editeur, int nombreCD, Person person, Person author2) throws CustomException {
		super(titre, editeur, person, "livreInfo");

		this.setMaxAuthorNb(2);
		try {
			addAuthor(author2);
		} catch (CustomException e) {
			e.printStackTrace();
		}
		
		if ( nombreCD < 1 ) {
			throw new CustomException( "ebooks needs 1 CD at least" );
		}
		this.nbCD = nombreCD;
	}
	
}
