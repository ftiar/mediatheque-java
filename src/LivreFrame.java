import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import article.Livre;

import javax.swing.JLabel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import person.Person;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class LivreFrame extends JFrame {
	private JTextField fieldTitle;
	private JTextField fieldEditeur;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LivreFrame frame = new LivreFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LivreFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 800);
		getContentPane().setLayout(new MigLayout("", "[100.00][100.00][100.00][100.00][100.00][100.00][100.00][100.00][100.00][100.00][100.00]", "[100.00][100.00][100.00][100.00][100.00][100.00][100.00][100.00][100.00][100.00][100.00]"));
		
		JLabel titleLabel = new JLabel("Titre");
		titleLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		getContentPane().add(titleLabel, "cell 1 1");
		
		fieldTitle = new JTextField();
		fieldTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));
		getContentPane().add(fieldTitle, "cell 2 1 2 1,growx");
		fieldTitle.setColumns(10);
		
		JLabel labelAfterBookAdded = new JLabel("");
		labelAfterBookAdded.setFont(new Font("Tahoma", Font.PLAIN, 20));
		getContentPane().add(labelAfterBookAdded, "cell 7 1");

		JLabel editeurLabel = new JLabel("Editeur");
		editeurLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		getContentPane().add(editeurLabel, "cell 1 2");
		
		fieldEditeur = new JTextField();
		fieldEditeur.setFont(new Font("Tahoma", Font.PLAIN, 20));
		getContentPane().add(fieldEditeur, "cell 2 2 2 1,growx");
		fieldEditeur.setColumns(10);
		
		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Livre livre = new Livre (fieldTitle.getText(), fieldEditeur.getText(), new Person( "JK", "Rowling", "author" ) );
				labelAfterBookAdded.setEnabled(true);
				labelAfterBookAdded.setText("Le livre : '" + livre.getTitre() + "' a �t� cr�� avec succ�s.");
			}
		});
		getContentPane().add(btnAjouter, "cell 1 3 3 1,alignx center,aligny center");
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnActions = new JMenu("Actions");
		menuBar.add(mnActions);
		
		JMenuItem mntmAjouterUnArticle = new JMenuItem("Ajouter un article");
		mnActions.add(mntmAjouterUnArticle);
		
		JMenuItem mntmAjouterUnAuteur = new JMenuItem("Ajouter un auteur");
		mnActions.add(mntmAjouterUnAuteur);
		
		JMenuItem mntmAjouterUnAbonn = new JMenuItem("Ajouter un abonn\u00E9");
		mnActions.add(mntmAjouterUnAbonn);
	
	}

}
