package user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import article.Article;
import mysql.Connect;

public class User {

	private int userID;
	private String ville;
	private String adresse;
	private String nom;
	private String type;
	private ArrayList <Article> livesEmpruntes;
	private int nb_livre_empruntable;
	
	public User(String ville, String adresse, String nom, String type) {
		this.ville = ville;
		this.adresse = adresse;
		this.nom = nom;
		this.livesEmpruntes = new ArrayList <Article>();
		
		if ( type == "student" ) {
			this.nb_livre_empruntable = 5;
		} else if ( type == "teacher" ) {
			this.nb_livre_empruntable = 10;
		}
		
		try {
			Connection cnx = Connect.getConnexion();

			PreparedStatement pst = null;
			ResultSet resultats = null;
			int idGenere = -1;
	        
	        String requeteSQL = "INSERT INTO user (city, address, name, type, bookBorrowable) values(?, ?, ?, ?, ?)";

	        System.out.println("before user creation");
	        pst = cnx.prepareStatement(requeteSQL, Statement.RETURN_GENERATED_KEYS);
	    
	        pst.setString(1, ville);
	        pst.setString(2, adresse);
	        pst.setString(3, nom);
	        pst.setString(4, type);
	        pst.setInt(5, nb_livre_empruntable);
                
	        try {
	            pst.executeUpdate();
	            resultats = pst.getGeneratedKeys();
	            if (resultats.next()) {
	            	idGenere = resultats.getInt(1);
	            	this.userID = idGenere;
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
    public static int deleteUser(int  id) throws SQLException, ClassNotFoundException {
		Connection cnx = Connect.getConnexion();
        PreparedStatement pst = null;
        int nblignes = 0;

        String requeteSQL = "DELETE from user where userID ="+id;

        System.out.println("before delete user");
        pst = cnx.prepareStatement(requeteSQL);   
            
        try {
            nblignes = pst.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        pst.close();
        cnx.close();
        return nblignes;
    }

	public int getUserID() {
		return userID;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList <Article> getLivesEmpruntes() {
		return this.livesEmpruntes;
	}

	public void showLivesEmpruntes() {
		System.out.println( "Livres empruntÚs" );
		for( int i = 0; i < this.livesEmpruntes.size(); i++ ) {
			System.out.println( this.livesEmpruntes.get(i).getTitre() );
		}
	}
	
	public boolean emprunteLivre(Article livre) {
		if ( ! livre.isDisponible() || this.livesEmpruntes.size() > this.nb_livre_empruntable ) {
			return false;
		}

		livre.setDisponibilite(false);
		this.livesEmpruntes.add(livre);
		
		// Add it to the database
		try {
			Connection cnx = Connect.getConnexion();

			PreparedStatement pst = null;
			ResultSet resultats = null;
			int idGenere = -1;
	        
	        String requeteSQL = "INSERT INTO userarticle (userID, articleID) values(?, ?)";

	        System.out.println("before user article creation");
	        pst = cnx.prepareStatement(requeteSQL, Statement.RETURN_GENERATED_KEYS);

	        pst.setInt(1, this.userID);
	        pst.setInt(2, livre.getArticleID());
                
	        try {
	            pst.executeUpdate();
	            resultats = pst.getGeneratedKeys();
	            if (resultats.next()) {
	            	idGenere = resultats.getInt(1);
	            	this.userID = idGenere;
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public boolean rendreLivre(Article livre) {
		if ( livre.isDisponible() ) {
			return false;
		}

		livre.setDisponibilite(true);
		this.livesEmpruntes.remove(livre);
		
		// Remove from DB
		Connection cnx;
		try {
			cnx = Connect.getConnexion();

	        PreparedStatement pst = null;

	        String requeteSQL = "DELETE from userarticle where articleID ="+livre.getArticleID() + " AND personID = " + this.getUserID();

	        System.out.println("avant de rendre un livre");
	        pst = cnx.prepareStatement(requeteSQL);   
	            
	        try {
	            pst.executeUpdate();
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", ville=" + ville + ", adresse=" + adresse + ", nom=" + nom
				+ ", livesEmpruntes=" + livesEmpruntes + "]";
	}
	
}
