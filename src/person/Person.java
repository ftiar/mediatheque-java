package person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import mysql.Connect;

public class Person {

	protected String firstName;
	protected String lastName;
	protected String personType;
	protected int workNumber;
	private int personID;

	public Person( String firstName, String lastName, String personType ) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.personType = personType;
		
		this.insertPerson(firstName, lastName, personType);
	}
	
	public int insertPerson( String firstName, String lastName, String personType ) {
		PreparedStatement pst = null;
		ResultSet resultats = null;
		int idGenere = -1;
		
		try {
			Connection cnx = Connect.getConnexion();
			
	        String requeteSQL = "INSERT INTO person (firstname, lastname, workmade, personType) values(?, ?, ?, ?)";

	        System.out.println("before person creation");
	        pst = cnx.prepareStatement(requeteSQL, Statement.RETURN_GENERATED_KEYS);
	    
	        pst.setString(1, firstName);
	        pst.setString(2, lastName);
	        pst.setInt(3, 0);
	        pst.setString(4, personType);

	        try {
	            pst.executeUpdate();
	            resultats = pst.getGeneratedKeys();
	            if (resultats.next()) {
	            	idGenere = resultats.getInt(1);
	            	this.personID = idGenere;
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return idGenere;
	}
	
    public int deletePerson(int  id) throws SQLException, ClassNotFoundException {
		Connection cnx = Connect.getConnexion();
        PreparedStatement pst = null;
        int nblignes = 0;

        String requeteSQL = "DELETE from person where personID ="+id;

        System.out.println("before delete person");
        pst = cnx.prepareStatement(requeteSQL);   
            
        try {
            nblignes = pst.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        pst.close();
        cnx.close();
        return nblignes;
    }
    
	 public static ResultSet GetPersonList() throws ClassNotFoundException, SQLException
     {
         String req = "select * from person";
 		 Connection cnx = Connect.getConnexion();
       
         PreparedStatement pst = cnx.prepareStatement(req);
         ResultSet jeu = pst.executeQuery();
         
         return jeu;
     }
	 
	 public static ResultSet GetPersonListByLastname(String nom) throws ClassNotFoundException, SQLException
     {
         String req = "select * from person where lastname= ? " ;
 		 Connection cnx = Connect.getConnexion();
       
         PreparedStatement pst = cnx.prepareStatement(req);
         
		 pst.setString(1, nom);

         ResultSet jeu = pst.executeQuery();
         
         return jeu;
     }
	 
	 public static ResultSet GetPersonListByFirstname(String nom) throws ClassNotFoundException, SQLException
     {
         String req = "select * from person where firstname= ? " ;
 		 Connection cnx = Connect.getConnexion();
       
         PreparedStatement pst = cnx.prepareStatement(req);
         
		 pst.setString(1, nom);

         ResultSet jeu = pst.executeQuery();
         
         return jeu;
     }

	 public static ResultSet GetPersonListByType(String type) throws ClassNotFoundException, SQLException
     {
         String req = "select * from person where personType= ? " ;
 		 Connection cnx = Connect.getConnexion();
       
         PreparedStatement pst = cnx.prepareStatement(req);
         
		 pst.setString(1, type);

         ResultSet jeu = pst.executeQuery();
         
         return jeu;
     }
	
	public int getPersonID() {
		return personID;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public int getWorkNumber() {
		return this.workNumber;
	}
	
	public void decreaseWorkNumber() {
		this.workNumber--;
		this.updateWorkNumber();
	}

	public void increaseWorkNumber() {
		this.workNumber++;
		this.updateWorkNumber();
		
	}
	public void updateWorkNumber() {
		PreparedStatement pst = null;
		try {
			Connection cnx = Connect.getConnexion();
			
	        String requeteSQL = "UPDATE person SET workmade=" + this.workNumber + " WHERE personID = " + this.personID;

	        System.out.println("before person creation");
	        pst = cnx.prepareStatement(requeteSQL);

	        try {
	            pst.executeUpdate();
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        }
	        pst.close();
	        cnx.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName + "]";
	}
	
}
