package mediatheque;

import java.sql.SQLException;

import article.Livre;
import article.LivreInfo;
import person.Person;
import user.User;

public class Main {

	public static void main(String[] args) {

		Person jkrowling = new Person( "JK.", "Rowling", "author" );
		Person rostand = new Person( "Jean", "Rostand", "author" );
		//Person bpitt = new Person( "Brad", "Pitt", "actor" );
		//Person sinatra = new Person( "Frank", "Sinatra", "singer" );
		
		User ftiar = new User( "Corbeil-Essonnes", "Route de Lisses", "Florian TIAR", "student" );
		User roget = new User( "Kremlin Bic�tre", "avenue g�n�rale de gaule", "Roget", "teacher" );
		
		Livre hpotter = new Livre( "Harry Potter", "Flamarion", jkrowling);
		System.out.println( hpotter );
		ftiar.emprunteLivre(hpotter);
		System.out.println( ftiar.getLivesEmpruntes() );
		
		try {
			hpotter.addAuthor( rostand );
		} catch (CustomException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println( hpotter.getAuthors() );
		try {
			LivreInfo hpotter_triologie = new LivreInfo( "Triologie Harry Potter", "Flamarion edition", 7, jkrowling);
			System.out.println(hpotter_triologie);
		} catch (CustomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
